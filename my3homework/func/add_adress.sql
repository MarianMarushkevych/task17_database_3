delimiter //
use storedpr_db //
drop function if exists joinName_adress //
 create function joinName_adress(valueId int)
 returns varchar(50)
 begin
 return(select concat(name, building_number) from apple_shop  where  id =valueId );
 end //
 
 delimiter ;
 
 select * ,joinName_adress(apple_shop_id)  from  employee;